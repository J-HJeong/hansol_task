# hansol_task

한솔그룹 직무부트캠프 과제

## 프로젝트 개요
* 개발 언어: JAVA (JDK 1.8)
* 프레임워크: Spring Boot 2.6.7
* DB: MySQL 8.0
* 빌드 도구: Maven
