import React from 'react';
import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import List from './components/js/AllData/List';
import PicList from './components/js/PicData/PicList'
import WorkList from './components/js/WorkData/WorkList'
import CompanyList from './components/js/CompanyData/CompanyList';
import CompanyPICList from './components/js/CompanyData/CompanyPICList';

  const App = () => {
    return(
      <div className='App'>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<List />} exact/>
            <Route path="/pic/list" element={<PicList />}/>
            <Route path="/work/list" element={<WorkList />}/>
            <Route path="/company/list" element={<CompanyList />}/>
            <Route path="/company/list/:companyname" element={<CompanyPICList/>}/>
          </Routes>
        </BrowserRouter>
      </div>
    );
};

export default App;