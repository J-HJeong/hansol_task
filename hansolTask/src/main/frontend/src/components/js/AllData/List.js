import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Header from '../Header';
import Tr from './Tr';
import Modal from 'react-modal';

const List = () => {
    const [info, setInfo] = useState([]);
    const [modalOn, setModalOn] = useState(false);

    const [work_code, setWork_code] = useState('');
    const [work_name, setWork_name] = useState('');
    const [company_name, setCompany_name] = useState(''); 
    const [name, setName] = useState('');
    const [position, setPosition] = useState('');
    const [work_content, setWork_content] = useState('');
    const [phone, setPhone] = useState('');

    useEffect(
        () => {
          axios({
              url: '/select',
              method: 'GET'
          }).then((res) => {
              setInfo(res.data);
          }).catch(err => console.log(err));
        }, []
    );

    const save = () => {
        setModalOn(true);
    }

    const handleSave = ()  => {
        axios({
            url: 'insert',
            method: 'POST',
            data: {
                work_code: work_code,
                work_name: work_name,
                company_name: company_name,
                name: name,
                position: position,
                work_content: work_content,
                phone: phone
            }
        }).then((res) => {
            setModalOn(false);
            alert('데이터가 입력되었습니다.');
        })
    }

    return(
        <>
          <Header></Header>
        <div className='border-b border-gray-200 shadow px-10'>
            
            <div className='text-xl font-bold mt-5 mb-3 text-center'>
                전체 데이터 조회
            </div>
            <div className='relative'>
                <button type="button" onClick={save} class="absolute bottom-0 right-0 text-white bg-gradient-to-br from-green-400 to-blue-600 hover:bg-gradient-to-bl focus:outline-none font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2">
                    데이터 입력</button>
            </div>
            <table className='min-w-full table-auto text-gray-800'>
                <thead className='justify-between'>
                    <tr className='bg-gray-800'>
                        <th className='text-gray-300 px-4 py-3'>업무코드</th>
                        <th className='text-gray-300 px-4 py-3'>업무명</th>
                        <th className='text-gray-300 px-4 py-3'>회사명</th>
                        <th className='text-gray-300 px-4 py-3'>이름</th>
                        <th className='text-gray-300 px-4 py-3'>직급</th>
                        <th className='text-gray-300 px-4 py-3'>종류</th>
                        <th className='text-gray-300 px-4 py-3'>전화번호</th>
                    </tr>
                </thead>
                <Tr info={info}/>
            </table>
            <footer class='w-full text-center border-t border-grey h-52'></footer>
        </div>
        <Modal isOpen={modalOn}>
   
        <div class='flex items-center justify-center min-h-screen from-teal-100 via-teal-300 to-teal-500 bg-gradient-to-br'>
		<div class='w-full max-w-lg px-10 py-8 mx-auto bg-white rounded-lg shadow-xl'>
			<div class='max-w-md mx-auto space-y-6'>

				<form action="">
					<h2 class="text-2xl font-bold ">데이터 입력</h2>
					<hr class="my-6"/>
					<label class="uppercase text-sm font-bold opacity-70">업무코드</label>
					<input type="text" value={work_code} onChange={e => {setWork_code(e.target.value)}} class="p-3 mt-2 mb-4 w-full bg-slate-200 rounded border-2 border-slate-200 focus:border-slate-600 focus:outline-none"/>
					<label class="uppercase text-sm font-bold opacity-70">업무명</label>
					<input type="text" value={work_name} onChange={e => {setWork_name(e.target.value)}} class="p-3 mt-2 mb-4 w-full bg-slate-200 rounded"/>
					<label class="uppercase text-sm font-bold opacity-70">회사명</label>
					<input type="text" value={company_name} onChange={e => {setCompany_name(e.target.value)}} class="p-3 mt-2 mb-4 w-full bg-slate-200 rounded"/>
                    <label class="uppercase text-sm font-bold opacity-70">이름</label>
					<input type="text" value={name} onChange={e => {setName(e.target.value)}} class="p-3 mt-2 mb-4 w-full bg-slate-200 rounded"/>
                    <label class="uppercase text-sm font-bold opacity-70">직급</label>
					<input type="text" value={position} onChange={e => {setPosition(e.target.value)}} class="p-3 mt-2 mb-4 w-full bg-slate-200 rounded border-2 border-slate-200 focus:border-slate-600 focus:outline-none"/>
                    <label class="uppercase text-sm font-bold opacity-70">종류</label>
					<input type="text" value={work_content} onChange={e => {setWork_content(e.target.value)}} class="p-3 mt-2 mb-4 w-full bg-slate-200 rounded border-2 border-slate-200 focus:border-slate-600 focus:outline-none"/>
                    <label class="uppercase text-sm font-bold opacity-70">전화번호</label>
					<input type="text" value={phone} onChange={e => {setPhone(e.target.value)}} class="p-3 mt-2 mb-4 w-full bg-slate-200 rounded border-2 border-slate-200 focus:border-slate-600 focus:outline-none"/>

					<input type="submit" onClick={handleSave} class="py-3 px-6 my-2 bg-emerald-500 text-white font-medium rounded hover:bg-indigo-500 cursor-pointer ease-in-out duration-300" value="Send"/>
				</form>

			</div>
		</div>
	</div>
        </Modal>
        </>
    );
};

export default List;