import React from 'react';

const Td = ({item}) => {
    return (
        <tr className='bg-white border-2 border-gray-200'>
            <td className='px-4' py-3>{item.work_code}</td>
            <td className='px-4' py-3>{item.work_name}</td>
            <td className='px-4' py-3>{item.company_name}</td>
            <td className='px-4' py-3>{item.pic_name}</td>
            <td className='px-4' py-3>{item.position}</td>
            <td className='px-4' py-3>{item.work_content}</td>
            <td className='px-4' py-3>{item.phone}</td>
        </tr>
    )
};

export default Td;