import React from 'react';

const Td = ({item}) => {
    return (
        <tr className='bg-white border-2 border-gray-200'>
            <td className='px-4 border-2' py-3>{item.work_code}</td>
            <td className='px-4 border-2' py-3>{item.work_name}</td>
        </tr>
    )
};

export default Td;