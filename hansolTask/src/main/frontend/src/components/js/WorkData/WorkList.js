import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Header from '../Header';
import Tr from './Tr';

const WorkList = () => {
    const [info, setInfo] = useState([]);

    useEffect(
        () => {
          axios({
              url: '/work/list',
              method: 'GET'
          }).then((res) => {
              setInfo(res.data);
          }).catch(err => console.log(err));
        }, []
    );

    return(
        <div>
          <Header></Header>
        <div className='border-gray-200  px-10'>
            <div className='text-xl font-bold mt-5 mb-3 text-center'>
                업무 목록 조회
            </div>
            <table className='min-w-full table-auto content-center text-gray-800 px-8'>
                <thead className='justify-between'>
                    <tr className='bg-gray-800'>
                        <th className='text-gray-300 px-4 py-3'>업무코드</th>
                        <th className='text-gray-300 px-4 py-3'>업무명</th>
                    </tr>
                </thead>
                <Tr info={info}/>
            </table>
            <footer class='w-full text-center border-t border-grey h-52'></footer>
        </div>
        </div>
    );
};

export default WorkList;