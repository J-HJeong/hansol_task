import React from "react";
import Td from "./Td";

const Tr = ({info}) => {
    return(
        <tbody>{
            info.map(item => {
                return(
                    <Td key={item.work_code} item={item}/>
                )
            })
        }
        </tbody>
    );
};

export default Tr;