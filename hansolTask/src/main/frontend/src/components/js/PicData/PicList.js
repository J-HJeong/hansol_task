import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Header from '../Header';
import Tr from './Tr';
import Modal from 'react-modal'

const PicList = (item) => {
    const [info, setInfo] = useState([]);
    const [modalOn, setModalOn] = useState(false);

    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [position, setPosition] = useState('');
    const [work_content, setWork_content] = useState('');
    const [phone, setPhone] = useState('');

    useEffect(
        () => {
          axios({
              url: '/pic/list',
              method: 'GET'
          }).then((res) => {
              setInfo(res.data);
          }).catch(err => console.log(err));
        }, []
    );

    const handleRemove = (id) => {
        axios({
            url: '/pic/delete/' + id,
            method: 'POST'
        }).then((res) => {
            alert('담당자가 삭제되었습니다.');
            window.location.reload();
        }).catch(err => console.log(err));
    }


    const handleEdit = (item) => {
        setModalOn(true);

        setId(item.id);
        setName(item.name);
        setPosition(item.position);
        setWork_content(item.work_content);
        setPhone(item.phone);
    };

    const handleEditSubmit = () => {
        console.log(item);
        axios({
            url: '/pic/update/' + id,
            method: 'POST',
            data: {
                name: name,
                position: position,
                work_content: work_content,
                phone: phone,
            }
        }).then((res) => {
            alert('담당자가 수정되었습니다.')
            setModalOn(false);
        }).catch(err => console.log(err));
    }

    return(
        <>
          <Header></Header>
        <div className='border-b border-gray-200 shadow px-10'>
            <div className='text-xl font-bold mt-5 mb-3 text-center'>
                담당자 목록 조회
            </div>
            <table className='min-w-full table-auto text-gray-800'>
                <thead className='justify-between'>
                    <tr className='bg-gray-800'>
                        <th className='text-gray-300 px-4 py-3'>ID</th>
                        <th className='text-gray-300 px-4 py-3'>이름</th>
                        <th className='text-gray-300 px-4 py-3'>직급</th>
                        <th className='text-gray-300 px-4 py-3'>종류</th>
                        <th className='text-gray-300 px-4 py-3'>전화번호</th>
                        <th className='text-gray-300 px-4 py-3'>업무코드</th>
                        <th className='text-gray-300 px-4 py-3'>Edit</th>
                        <th className='text-gray-300 px-4 py-3'>Delete</th>
                    </tr>
                </thead>
                <Tr info={info} handleRemove={handleRemove} handleEdit={handleEdit}/>
            </table>
            <footer class='w-full text-center border-t border-grey h-52'></footer>
        </div>
        <Modal isOpen={modalOn}>
   
        <div class='flex items-center justify-center min-h-screen from-teal-100 via-teal-300 to-teal-500 bg-gradient-to-br'>
		<div class='w-full max-w-lg px-10 py-8 mx-auto bg-white rounded-lg shadow-xl'>
			<div class='max-w-md mx-auto space-y-6'>

				<form action="">
					<h2 class="text-2xl font-bold ">담당자 수정</h2>
					<hr class="my-6"/>
					<label class="uppercase text-sm font-bold opacity-70">이름</label>
					<input type="text" defaultValue={name} onChange={e => {setName(e.target.value)}} class="p-3 mt-2 mb-4 w-full bg-slate-200 rounded border-2 border-slate-200 focus:border-slate-600 focus:outline-none"/>
					<label class="uppercase text-sm font-bold opacity-70">직급</label>
					<input type="text" defaultValue={position} onChange={e => {setPosition(e.target.value)}} class="p-3 mt-2 mb-4 w-full bg-slate-200 rounded"/>
					<label class="uppercase text-sm font-bold opacity-70">종류</label>
					<input type="text" defaultValue={work_content} onChange={e => {setWork_content(e.target.value)}} class="p-3 mt-2 mb-4 w-full bg-slate-200 rounded"/>
                    <label class="uppercase text-sm font-bold opacity-70">전화번호</label>
					<input type="text" defaultValue={phone} onChange={e => {setPhone(e.target.value)}} class="p-3 mt-2 mb-4 w-full bg-slate-200 rounded"/>

					<input type="submit" onClick={handleEditSubmit} class="py-3 px-6 my-2 bg-emerald-500 text-white font-medium rounded hover:bg-indigo-500 cursor-pointer ease-in-out duration-300" value="Send"/>
				</form>

			</div>
		</div>
	</div>
        </Modal>
        </>
    );
};

export default PicList;