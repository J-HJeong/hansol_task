import React from 'react';

const Td = ({item, handleRemove, handleEdit}) => {

    const onRemove = () => {
        handleRemove(item.id);
    }

    const onEdit = () => {
        handleEdit(item);
    }

    return (
        <>
        <tr className='bg-white border-2 border-gray-200'>
            <td className='px-4' py-3>{item.id}</td>
            <td className='px-4' py-3>{item.name}</td>
            <td className='px-4' py-3>{item.position}</td>
            <td className='px-4' py-3>{item.work_content}</td>
            <td className='px-4' py-3>{item.phone}</td>
            <td className='px-4' py-3>{item.work_code}</td>
            <td className='px-4' py-3>
                <button onClick={onEdit} type='button' className='center data-modal-toggle="authentication-modal"'>
                    <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z"></path></svg>
                </button>
            </td>
            <td className='px-4' py-3>
                <button onClick={onRemove} type='button' className='center'>
                    <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg>
                </button>            
            </td>
        </tr>
        </>
    )
};

export default Td;