import React from "react";

const Header = () => {
  return (
   <nav className="bg-gray-100">
       <div className="max-w-full mx-auto px-4 border">
        <div className="flex justify-between">

        {/* 메뉴 */}
        <div className="flex space-x-4">
            <div>
                <a href="/" className="flex items-center py-5 px-2 text-gray-700">
                <span className="font-bold">전체</span>
              </a>
            </div>
            <div>
                <a href="/pic/list" className="flex items-center py-5 px-2 text-gray-700">
                    담당자
                </a>
            </div>
            <div>
                <a href="/work/list" className="flex items-center py-5 px-2 text-gray-700">
                    업무
                </a>
            </div>
            <div>
                <a href="/company/list" className="flex items-center py-5 px-2 text-gray-700">
                    회사
                </a>
            </div>
        </div>
        </div>
        </div>  
   </nav>
  );
};
export default Header;