import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Header from '../Header';
import Tr from './Tr_List';

const CompanyList = () => {
    const [info, setInfo] = useState([]);

    useEffect(
        () => {
          axios({
              url: '/company/count',
              method: 'GET'
          }).then((res) => {
              setInfo(res.data);
          }).catch(err => console.log(err));
        }, []
    );

    return(
        <div>
          <Header></Header>
        <div className='align-middle px-10'>
            <div className='text-xl font-bold mt-5 mb-3 text-center'>
                회사 당 담당자 수 조회
            </div>
            <table className='min-w-full table-auto text-gray-800'>
                <thead className='justify-between'>
                    <tr className='bg-gray-800'>
                        <th className='text-gray-300 px-4 py-3'>회사명</th>
                        <th className='text-gray-300 px-4 py-3'>담당자 수</th>
                    </tr>
                </thead>
                <Tr info={info}/>
            </table>
        </div>
        </div>
    );
};

export default CompanyList;