import React from 'react';

const Td_PICList = ({item}) => {
    return (
        <tr className='bg-white border-2 border-gray-200'>
            <td className='px-4' py-3>{item.id}</td>
            <td className='px-4' py-3>{item.name}</td>
            <td className='px-4' py-3>{item.position}</td>
        </tr>
    )
};

export default Td_PICList;