import React from 'react';
import { Link } from 'react-router-dom';

const Td_List = ({item}) => {
    const url = '/company/list/' + item.company_name;

    return (
        <tr className='bg-white border-2 border-gray-200'>
            <td className='px-4 border-2' py-3>
                <Link to={url}>{item.company_name}</Link>
            </td>
            <td className='px-4 border-2' py-3>{item.count}</td>
        </tr>
    )
};

export default Td_List;