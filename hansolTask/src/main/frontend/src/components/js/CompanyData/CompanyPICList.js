import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Header from '../Header';
import { useParams } from 'react-router-dom';
import Tr from './Tr_PICList';

const CompanyPICList = ({ }) => {
    const [info, setInfo] = useState([]);

    const {companyname} = useParams();

    useEffect(
        () => {
          axios({
              url: '/company/pic-list/' + companyname,
              method: 'GET'
          }).then((res) => {
              setInfo(res.data);
          }).catch(err => console.log(err));
        }, []
    );

    return(
        <div>
          <Header></Header>
        <div className='align-middle px-10'>
            <div className='text-xl font-bold mt-5 mb-3 text-center'>
                {companyname}의 담당자 목록 조회
            </div>
            <table className='min-w-full table-auto text-gray-800'>
                <thead className='justify-between'>
                    <tr className='bg-gray-800'>
                        <th className='text-gray-300 px-4 py-3'>ID</th>
                        <th className='text-gray-300 px-4 py-3'>담당자 이름</th>
                        <th className='text-gray-300 px-4 py-3'>직급</th>

                    </tr>
                </thead>
                <Tr info={info}/>
            </table>
        </div>
        </div>
    );
};

export default CompanyPICList;