import React from "react";
import Td from "./Td_PICList";

const Tr_PICList = ({info}) => {
    return(
        <tbody>{
            info.map(item => {
                return(
                    <Td key={item.id} item={item}/>
                )
            })
        }
        </tbody>
    );
};

export default Tr_PICList;