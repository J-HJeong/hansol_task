import React from "react";
import Td from "./Td_List";

const Tr_List = ({info}) => {
    return(
        <tbody>{
            info.map(item => {
                return(
                    <Td key={item.company_name} item={item}/>
                )
            })
        }
        </tbody>
    );
};

export default Tr_List;