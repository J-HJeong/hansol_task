package com.hansolTask.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@JsonAutoDetect(fieldVisibility=Visibility.ANY)
@Data
public class WorkDTO {
	@ApiModelProperty(value="업무코드", example="업무코드")
	private String work_code;
	
	@ApiModelProperty(value="업무명", example="업무명")
	private String work_name;

	public WorkDTO(String work_code, String work_name) { 
		this.work_code = work_code; 
		this.work_name = work_name; 
	}
	 
	
}
