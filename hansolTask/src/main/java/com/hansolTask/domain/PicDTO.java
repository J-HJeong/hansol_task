package com.hansolTask.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@JsonAutoDetect(fieldVisibility=Visibility.ANY)
public class PicDTO {
	@ApiModelProperty(value="담당자 아이디", example="담당자 아이디")
	private int id;
	
	@ApiModelProperty(value="담당자 이름", example="담당자 이름")
	private String name;
	
	@ApiModelProperty(value="담당자 직급", example="담당자 직급")
	private String position;
	
	@ApiModelProperty(value="담당자 업무 종류", example="담당자 업무 종류")
	private String work_content;
	
	@ApiModelProperty(value="담당자 전화번호", example="담당자 전화번호")
	private String phone;
	
	@ApiModelProperty(value="담당자의 업무 코드", example="담당자의 업무 코드")
	private String work_code;
	
//	public PicDTO(String name, String position, String work_content, String phone
//			) { 
//		this.name = name; 
//		this.position = position;
//		this.work_content = work_content; 
//		this.phone = phone; 
//	}
	
	//public PicDTO() {}
}