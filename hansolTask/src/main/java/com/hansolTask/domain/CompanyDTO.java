package com.hansolTask.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@JsonAutoDetect(fieldVisibility=Visibility.ANY)
@Data
public class CompanyDTO {
	@ApiModelProperty(value="회사 아이디", example="회사 아이디")
	private int company_id;
	
	@ApiModelProperty(value="회사명", example="회사명")
	private String company_name;
	
	@ApiModelProperty(value="담당자 아이디", example="담당자 아이디")
	private int pic_id;
	
	@ApiModelProperty(value="회사당 담당자 수", example="회사당 담당자 수")
	private int count;
	
	
	public CompanyDTO(String name, int pic_id) { 
		this.company_name = name;
		this.pic_id = pic_id; 
	}
	 
	
}
