package com.hansolTask.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AllMapper {
	// 전체 데이터 조회
	public List<Map<String, Object>> getAllData();
}
