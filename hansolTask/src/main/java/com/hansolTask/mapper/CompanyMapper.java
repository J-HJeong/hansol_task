package com.hansolTask.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import com.hansolTask.domain.CompanyDTO;
import com.hansolTask.domain.PicDTO;

@Mapper
public interface CompanyMapper {
	// 회사당 담당자 수 조회
	public List<CompanyDTO> getCompanyCount();
	
	// 회사 테이블 입력
	public void insert(CompanyDTO companyDTO);
	
	public void delete(int id);
	
	public void alter();
	
	public void setCnt();
	
	public void setColumn();
	
	public void deleteAll();
	
	public List<PicDTO> getCompanyPICList(String companyName);
	
	public void updatePICID(int pic_id);
}
