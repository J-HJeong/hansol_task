package com.hansolTask.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import com.hansolTask.domain.WorkDTO;

@Mapper
public interface WorkMapper {
	// 업무 목록 조회
	public List<WorkDTO> getWorkList();
	
	// 업무 테이블 입력
	public void insert(WorkDTO workDTO);
	
	public int getWorkCodeCount(String workCode);
	
	public void deleteAll();
}
