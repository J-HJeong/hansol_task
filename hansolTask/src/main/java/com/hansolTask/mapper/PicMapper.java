package com.hansolTask.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import com.hansolTask.domain.PicDTO;

@Mapper
public interface PicMapper {
	// 담당자 개인 정보 목록 조회
	public List<PicDTO> getPICList();
	
	// 담당자 테이블 입력
	public void insert(PicDTO picDTO);
	
	public int getCount();
	
	// 담당자 이름으로 삭제
	public void delete(int id);
	
	public void alter();
	
	public void setCnt();
	
	public void setColumn();
	
	// 담당자의 직급 수정
	//public void updatePosition(String position, int id);
	public void update(int id, PicDTO picDTO);
	
	public void deleteAll();
}
