package com.hansolTask;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan(basePackageClasses=HansolTaskApplication.class)
@SpringBootApplication
public class HansolTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(HansolTaskApplication.class, args);
	}

}
