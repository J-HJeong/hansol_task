package com.hansolTask.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hansolTask.domain.WorkDTO;
import com.hansolTask.service.WorkService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;

@Api(tags= {"Work Controller"})
@RestController
@RequestMapping(value="/work")
public class WorkController {
	@Autowired
	private WorkService workServiceImpl;
	
	@ApiResponse(code=200, message="업무 목록 조회 성공")
	@GetMapping("/list")
	@ApiOperation(value="업무 조회", notes="업무 목록 조회 API")
	public List<WorkDTO> getWorkList(){
		return workServiceImpl.getWorkList();
	}
}
