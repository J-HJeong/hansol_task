package com.hansolTask.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hansolTask.domain.CompanyDTO;
import com.hansolTask.domain.PicDTO;
import com.hansolTask.domain.WorkDTO;
import com.hansolTask.service.AllService;
import com.hansolTask.service.CompanyService;
import com.hansolTask.service.PicService;
import com.hansolTask.service.WorkService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.Data;

@Api(tags= {"All Controller"})
@RestController
@RequestMapping(value="/")
public class AllController {
	@Autowired
	private CompanyService companyServiceImpl;
	
	@Autowired
	private PicService picServiceImpl;
	
	@Autowired
	private WorkService workServiceImpl;
	
	@Autowired
	private AllService allServiceImpl;
	
//	@ApiImplicitParams({
//        @ApiImplicitParam(name="work_code", value="업무코드", required=true, dataType="String")
//        , @ApiImplicitParam(name="work_name", value="업무명", required=true, dataType="String")
//        , @ApiImplicitParam(name="company_name", value="회사명", required=true, dataType="String")
//        , @ApiImplicitParam(name="name", value="담당자 이름", required=true, dataType="String")
//        , @ApiImplicitParam(name="position", value="직급", required=true, dataType="String")
//        , @ApiImplicitParam(name="work_content", value="종류", required=false, dataType="String")
//        , @ApiImplicitParam(name="phone", value="전화번호", required=true, dataType="String")
//	})
	@ApiResponse(code=200, message="입력 성공")
	@PostMapping("/insert")
	@ApiOperation(value="입력", notes="입력 API")
//	public void insert(@RequestParam String work_code, @RequestParam String work_name, 
//			@RequestParam String company_name, @RequestParam String name, @RequestParam String position, 
//			@RequestParam(required=false) String work_content, @RequestParam String phone){
//		WorkDTO work = new WorkDTO(work_code, work_name);
	public void insert(@RequestBody AllData data) {
//		if(workServiceImpl.getWorkCodeCount(work_code) == 0)
//			workServiceImpl.insert(work);
//		
//		PicDTO pic = new PicDTO(0, name, position, work_content, phone, work_code); 
//		int pic_id = picServiceImpl.insert(pic);
//		  
//		if(company_name.contains(",")) { 
//			String[] companyNames = company_name.split(","); 
//			for(String companyName : companyNames) { 
//				CompanyDTO company = new CompanyDTO(companyName.trim(), pic_id);
//				companyServiceImpl.insert(company); 
//			} 
//		}
//		else {
//			CompanyDTO company = new CompanyDTO(company_name, pic_id);
//			companyServiceImpl.insert(company); 
//		}
	
		if(workServiceImpl.getWorkCodeCount(data.getWork_code()) == 0) {
			WorkDTO work = new WorkDTO(data.getWork_code(), data.getWork_name());
			workServiceImpl.insert(work);
		}
		
		PicDTO pic = new PicDTO(0, data.getName(), data.getPosition(), data.getWork_content(), data.getPhone(), data.getWork_code());
		int pic_id = picServiceImpl.insert(pic);
		
		String company_name = data.getCompany_name();
		
		if(company_name.contains(",")) { 
			String[] companyNames = company_name.split(","); 
			for(String companyName : companyNames) { 
				CompanyDTO company = new CompanyDTO(companyName.trim(), pic_id);
				companyServiceImpl.insert(company); 
			} 
		}
		else {
			CompanyDTO company = new CompanyDTO(company_name, pic_id);
			companyServiceImpl.insert(company); 
		}
	}
	
	@ApiResponse(code=200, message="전체 데이터 조회 성공")
	@GetMapping("/select")
	@ApiOperation(value="전체 데이터 조회", notes="전체 데이터 조회 API")
	public List<Map<String, Object>> getAllData(){
		return allServiceImpl.getAllData();
	}
	
	@ApiResponse(code=200, message="전체 데이터 삭제 성공")
	@PostMapping("/delete")
	@ApiOperation(value="전체 데이터 삭제", notes="전체 데이터 삭제 API")
	public void deleteAll(){
		// 회사 -> 담당자 -> 업무 순으로 삭제
		companyServiceImpl.deleteAll();
		picServiceImpl.deleteAll();
		workServiceImpl.deleteAll();
	}
	
	@Data
	public static class AllData{
		private String work_code;
		private String work_name;
		private String company_name;
		private String name;
		private String position;
		private String work_content;
		private String phone;
	}
}
