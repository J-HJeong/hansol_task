package com.hansolTask.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hansolTask.domain.PicDTO;
import com.hansolTask.service.CompanyService;
import com.hansolTask.service.PicService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.Data;

@Api(tags= {"PIC Controller"})
@RestController
@RequestMapping(value="/pic")
public class PicController {
	@Autowired
	private PicService picServiceImpl;
	@Autowired
	private CompanyService companyServiceImpl;
	
	@ApiResponse(code=200, message="담당자 목록 조회 성공")
	@GetMapping("/list")
	@ApiOperation(value="담당자 조회", notes="담당자 목록 조회 API")
	public List<PicDTO> getPICList(){
		return picServiceImpl.getPICList();
	}
	
	@ApiResponse(code=200, message="담당자 삭제 성공")
	@ApiImplicitParam(name="id", value="삭제할 담당자 ID", example="0", required=true, dataType="Integer")
	@PostMapping("/delete/{id}")
	@ApiOperation(value="담당자 삭제", notes="담당자 삭제 API") 
	public void delete(@PathVariable int id) { 
		companyServiceImpl.delete(id);
		picServiceImpl.delete(id);
		companyServiceImpl.updatePICID(id);
	}
	 
	@PostMapping("/update/{id}")
	@ApiOperation(value="담당자 수정", notes="담당자 수정 API") 
	public void update(@PathVariable int id, @RequestBody PIC pic) {
				picServiceImpl.update(id, pic.getName(), pic.getPosition(), pic.getWork_content(), pic.getPhone());
	}
	
	@Data
	static class PIC {
		private String name;
		private String position;
		private String work_content;
		private String phone;
	}
}


