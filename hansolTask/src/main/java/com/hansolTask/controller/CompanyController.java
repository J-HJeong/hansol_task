package com.hansolTask.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hansolTask.domain.CompanyDTO;
import com.hansolTask.domain.PicDTO;
import com.hansolTask.service.CompanyService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;

@Api(tags= {"Company Controller"})
@RestController
@RequestMapping(value="/company")
public class CompanyController {
	
	@Autowired
	private CompanyService companyServiceImpl;
	
	@ApiResponse(code=200, message="회사 당 담당자 수 조회 성공")
	@GetMapping("/count")
	@ApiOperation(value="회사 조회", notes="회사 당 담당자 수 조회 API")
	public List<CompanyDTO> getCompanyCount(){
		return companyServiceImpl.getCompanyCount();
	}
	
	@ApiResponse(code=200, message="회사 당 담당자 목록 조회 성공")
	@GetMapping("/pic-list/{companyName}")
	@ApiOperation(value="회사 당 담당자 목록 조회", notes="회사 당 담당자 목록 조회 API")
	public List<PicDTO> getCompanyPICList(@PathVariable("companyName") String companyName){
		return companyServiceImpl.getCompanyPICList(companyName);
	}
}
