package com.hansolTask.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hansolTask.domain.WorkDTO;
import com.hansolTask.mapper.WorkMapper;

@Service
public class WorkServiceImpl implements WorkService {
	@Autowired
	private WorkMapper mapper;
	
	@Override
	public List<WorkDTO> getWorkList() {
		return mapper.getWorkList();
	}

	@Override
	public void insert(WorkDTO workDTO) {
		mapper.insert(workDTO);
	}

	@Override
	public int getWorkCodeCount(String workCode) {
		return mapper.getWorkCodeCount(workCode);
	}

	@Override
	public void deleteAll() {
		mapper.deleteAll();
	}


}
