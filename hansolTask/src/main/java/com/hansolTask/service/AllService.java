package com.hansolTask.service;

import java.util.List;
import java.util.Map;

public interface AllService {
	public List<Map<String, Object>> getAllData();
}
