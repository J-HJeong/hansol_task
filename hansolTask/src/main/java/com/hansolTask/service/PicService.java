package com.hansolTask.service;

import java.util.List;

import com.hansolTask.domain.PicDTO;

public interface PicService {
	public List<PicDTO> getPICList();
	public int insert(PicDTO picDTO);
	public void delete(int id);
	public void deleteAll();
	//public void updatePosition(String position, int id);
	public void update(int id, String name, String position, String work_content, String phone);
}
