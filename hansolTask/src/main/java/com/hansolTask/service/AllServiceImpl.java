package com.hansolTask.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hansolTask.mapper.AllMapper;

@Service
public class AllServiceImpl implements AllService {

	@Autowired
	private AllMapper allMapper;
	
	@Override
	public List<Map<String, Object>> getAllData() {
		return allMapper.getAllData();
	}

}
