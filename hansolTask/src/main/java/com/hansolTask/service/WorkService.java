package com.hansolTask.service;

import java.util.List;

import com.hansolTask.domain.WorkDTO;

public interface WorkService {
	public List<WorkDTO> getWorkList();
	public void insert(WorkDTO workDTO);
	public int getWorkCodeCount(String workCode);
	public void deleteAll();
}
