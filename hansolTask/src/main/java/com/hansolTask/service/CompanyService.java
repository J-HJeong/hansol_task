package com.hansolTask.service;

import java.util.List;

import com.hansolTask.domain.CompanyDTO;
import com.hansolTask.domain.PicDTO;

public interface CompanyService {
	public List<CompanyDTO> getCompanyCount();
	public void insert(CompanyDTO company);
	public void delete(int id);
	public void deleteAll();
	public List<PicDTO> getCompanyPICList(String companyName);
	public void updatePICID(int pic_id);
}
