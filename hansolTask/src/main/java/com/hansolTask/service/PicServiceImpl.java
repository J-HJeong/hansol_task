package com.hansolTask.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hansolTask.domain.PicDTO;
import com.hansolTask.mapper.PicMapper;

@Service
public class PicServiceImpl implements PicService {
	@Autowired
	private PicMapper mapper;
	
	@Override
	public List<PicDTO> getPICList() {
		return mapper.getPICList();
	}

	@Override
	public int insert(PicDTO picDTO) {
		mapper.insert(picDTO);
		return mapper.getCount();
	}

	@Override
	public void delete(int id) {
		mapper.delete(id);
		mapper.alter();
		mapper.setCnt();
		mapper.setColumn();
	}

	/*
	 * @Override public void updatePosition(String position, int id) {
	 * mapper.updatePosition(position, id); }
	 */

	@Override
	public void deleteAll() {
		mapper.deleteAll();
	}

	@Override
	public void update(int id, String name, String position, String work_content, String phone) {
		PicDTO picDTO = new PicDTO(id, name, position, work_content, phone, null);
		mapper.update(id, picDTO);
		
	}
	
}
