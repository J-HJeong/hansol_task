package com.hansolTask.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hansolTask.domain.CompanyDTO;
import com.hansolTask.domain.PicDTO;
import com.hansolTask.mapper.CompanyMapper;

@Service
public class CompanyServiceImpl implements CompanyService {
	@Autowired
	private CompanyMapper mapper;
	
	@Override
	public List<CompanyDTO> getCompanyCount() {
		return mapper.getCompanyCount();
	}

	@Override public void insert(CompanyDTO company) { 
		mapper.insert(company); 
	}

	@Override
	public void delete(int id) {
		mapper.delete(id);
		mapper.alter();
		mapper.setCnt();
		mapper.setColumn();
	}

	@Override
	public void deleteAll() {
		mapper.deleteAll();
	}

	@Override
	public List<PicDTO> getCompanyPICList(String companyName) {
		return mapper.getCompanyPICList(companyName);
	}

	@Override
	public void updatePICID(int pic_id) {
		mapper.updatePICID(pic_id);
	}
	 

}
